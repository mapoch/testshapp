package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    public WebDriver driver;
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div/div/form/button")
    private WebElement btnEnter;

    @FindBy(xpath = "/html/body/div/div/form/div[1]/input")
    private WebElement fldLogin;

    @FindBy(xpath = "/html/body/div/div/form/div[2]/input")
    private WebElement fldPassword;

    public void enterLogin(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldLogin.clear();
                fldLogin.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldLogin = driver.findElement(By
                        .xpath("/html/body/div/div/form/div[1]/input"));
            }
        }
    }

    public void enterPassword(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldPassword.clear();
                fldPassword.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldPassword = driver.findElement(By
                        .xpath("/html/body/div/div/form/div[1]/input"));
            }
        }
    }

    public void clickBtnEnter() { btnEnter.click(); }
}
