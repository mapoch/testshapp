package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.v110.input.Input;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CounterPage {
    public WebDriver driver;
    public CounterPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div/div/div[4]/input")
    private WebElement fldOutput;

    @FindBy(xpath = "/html/body/div/div/div[5]/a")
    private WebElement btnReturn;

    @FindBy(xpath = "/html/body/div/div/form/div[1]/input")
    private WebElement fldString;

    @FindBy(xpath = "/html/body/div/div/form/div[2]/select")
    private WebElement cbMode;

    @FindBy(xpath = "/html/body/div/div/form/button")
    private WebElement btnEnter;

    public void loadRes() {
        fldOutput = driver.findElement(By.xpath("/html/body/div/div/div[4]/input"));
        btnReturn = driver.findElement(By.xpath("/html/body/div/div/div[5]/a"));
    }

    public void loadInputs() {
        fldString = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/input"));
        cbMode = driver.findElement(By.xpath("/html/body/div/div/form/div[2]/select"));
    }

    public String getOutput() {
        return fldOutput.getAttribute("value");
    }

    public void clickBtnReturn() { btnReturn.click();}

    public void enterString(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldString.clear();
                fldString.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldString = driver.findElement(By
                        .xpath("/html/body/div/div/form/div[1]/input"));
            }
        }
    }

    public void selectMode(int id) {
        Select select = new Select(cbMode);
        select.selectByIndex(id);
    }

    public void clickBtnEnter() { btnEnter.click();}
}
