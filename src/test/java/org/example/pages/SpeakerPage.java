package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SpeakerPage {
    public WebDriver driver;
    public SpeakerPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div/div/p")
    private WebElement lblHello;

    @FindBy(xpath = "/html/body/div/div/form/div[1]/input")
    private WebElement fldName;

    @FindBy(xpath = "/html/body/div/div/form/div[2]/select")
    private WebElement cbLang;

    @FindBy(xpath = "/html/body/div/div/form/button")
    private WebElement btnEnter;

    public String getOutput() {
        return lblHello.getText();
    }

    public void enterName(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldName.clear();
                fldName.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldName = driver.findElement(By
                        .xpath("/html/body/div/div/form/div[1]/input"));
            }
        }
    }

    public void selectLang(int id) {
        Select select = new Select(cbLang);
        select.selectByIndex(id);
    }

    public void clickBtnEnter() { btnEnter.click(); }
}
