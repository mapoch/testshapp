package org.example.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StudentPage {
    public WebDriver driver;
    public StudentPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div/div/div[1]/a")
    private WebElement btnAdd;

    @FindBy(xpath = "/html/body/div/div/div[2]/table/tbody/tr/td[2]")
    private WebElement fRow;

    @FindBy(xpath = "/html/body/div/div/div[2]/table/tbody/tr/td[4]/div/button")
    private WebElement btnDeleteRow;

    @FindBy(xpath = "/html/body/div/div/form/div[1]/input")
    private WebElement fldFName;

    @FindBy(xpath = "/html/body/div/div/form/div[2]/input")
    private WebElement fldLName;

    @FindBy(xpath = "/html/body/div/div/form/div[4]/button")
    private WebElement btnEnter;

    public void loadInputs() {
        fldFName = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/input"));
        fldLName = driver.findElement(By.xpath("/html/body/div/div/form/div[2]/input"));
        btnEnter = driver.findElement(By.xpath("/html/body/div/div/form/div[4]/button"));
    }

    public void loadList() {
        btnAdd = driver.findElement(By.xpath("/html/body/div/div/div[1]/a"));
        fRow = driver.findElement(By.xpath("/html/body/div/div/div[2]/table/tbody/tr/td[2]"));
        btnDeleteRow = driver.findElement(By.xpath("/html/body/div/div/div[2]/table/tbody/tr/td[4]/div/button"));
    }

    public void clickBtnAdd() { btnAdd.click(); }

    public String getRow(int row) {
        try {
            fRow = driver.findElement(By.xpath("/html/body/div/div/div[2]/table/tbody/tr["+row+"]/td[2]"));
            return fRow.getText();
        } catch (StaleElementReferenceException | NoSuchElementException ex) {
            return "";
        }
    }

    public void clickBtnEnter() { btnEnter.click(); }

    public void clickBtnDeleteRow(int row) {
        btnDeleteRow = driver.findElement(By.xpath("/html/body/div/div/div[2]/table/tbody/tr["+row+"]/td[4]/div/button"));
        btnDeleteRow.click();
    }

    public void enterFName(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldFName.clear();
                fldFName.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldFName = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/input"));
            }
        }
    }

    public void enterLName(String input) {
        boolean flag = true;
        while (flag) {
            try {
                fldLName.clear();
                fldLName.sendKeys(input);
                flag = false;
            } catch (StaleElementReferenceException ex) {
                fldLName = driver.findElement(By.xpath("/html/body/div/div/form/div[2]/input"));
            }
        }
    }
}
