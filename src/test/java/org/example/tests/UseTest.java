package org.example.tests;
import org.example.ConfProperties;
import org.example.pages.*;
import org.junit.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class UseTest {
    public static LoginPage loginPage;
    public static SpeakerPage speakerPage;
    public static CounterPage counterPage;
    public static StudentPage studentPage;
    public static ProfessorPage professorPage;
    public static SubjectPage subjectPage;
    public static WebDriver driver;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.edge.driver", ConfProperties.getProperty("driver"));
        EdgeOptions options = new EdgeOptions();
        options.addArguments("--remote-allow-origins=*", "--headless");
        driver = new EdgeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get(ConfProperties.getProperty("login_page"));
        loginPage = new LoginPage(driver);
        loginPage.enterLogin(ConfProperties.getProperty("login"));
        loginPage.enterPassword(ConfProperties.getProperty("password"));
        loginPage.clickBtnEnter();
    }

    private static void waitSec() {
        try {
            new WebDriverWait(driver, Duration.ofSeconds(1)).until(ExpectedConditions.urlContains("xd"));
        }
        catch (Exception ex) {
        }
    }

    @Test
    public void speakerTest() {
        driver.get(ConfProperties.getProperty("speaker_page"));
        speakerPage = new SpeakerPage(driver);

        speakerPage.enterName("World");
        speakerPage.selectLang(1);
        speakerPage.clickBtnEnter();

        Assert.assertEquals("Hello World!", speakerPage.getOutput());

        speakerPage.enterName("World");
        speakerPage.selectLang(2);
        speakerPage.clickBtnEnter();

        Assert.assertEquals("Hallo World!", speakerPage.getOutput());
    }

    @Test
    public void counterTest() {
        driver.get(ConfProperties.getProperty("counter_page"));
        counterPage = new CounterPage(driver);

        counterPage.enterString("test");
        counterPage.selectMode(0);
        counterPage.clickBtnEnter();
        counterPage.loadRes();

        Assert.assertEquals("4", counterPage.getOutput());

        counterPage.clickBtnReturn();
        counterPage.loadInputs();
        counterPage.enterString("test");
        counterPage.selectMode(1);
        counterPage.clickBtnEnter();
        counterPage.loadRes();

        Assert.assertEquals("10", counterPage.getOutput());

        counterPage.clickBtnReturn();
        counterPage.loadInputs();
        counterPage.enterString("test");
        counterPage.selectMode(2);
        counterPage.clickBtnEnter();
        counterPage.loadRes();

        Assert.assertEquals("7", counterPage.getOutput());
    }

    @Test
    public void studentTest() {
        driver.get(ConfProperties.getProperty("student_page"));
        studentPage = new StudentPage(driver);

        studentPage.clickBtnAdd();
        studentPage.loadInputs();
        studentPage.enterFName("Anton");
        studentPage.enterLName("Petrov");
        studentPage.clickBtnEnter();
        studentPage.loadList();

        int row = 1;
        while (!studentPage.getRow(row).equals("")) {
            row++;
        }
        row--;

        Assert.assertEquals("Anton Petrov", studentPage.getRow(row));

        studentPage.clickBtnDeleteRow(row);
        Alert alert = driver.switchTo().alert();
        alert.accept();

        Assert.assertNotEquals("Anton Petrov", studentPage.getRow(row));
    }

    @Test
    public void professorTest() {
        driver.get(ConfProperties.getProperty("professor_page"));
        professorPage = new ProfessorPage(driver);

        professorPage.clickBtnAdd();
        professorPage.loadInputs();
        professorPage.enterFName("Anton");
        professorPage.enterLName("Petrov");
        professorPage.clickBtnEnter();
        professorPage.loadList();

        int row = 1;
        while (!professorPage.getRow(row).equals("")) {
            row++;
        }
        row--;

        Assert.assertEquals("Anton Petrov", professorPage.getRow(row));

        professorPage.clickBtnDeleteRow(row);
        Alert alert = driver.switchTo().alert();
        alert.accept();

        Assert.assertNotEquals("Anton Petrov", professorPage.getRow(row));
    }

    @Test
    public void subjectTest() {
        driver.get(ConfProperties.getProperty("subject_page"));
        subjectPage = new SubjectPage(driver);

        subjectPage.clickBtnAdd();
        subjectPage.loadInputs();
        subjectPage.enterName("SSPR");
        subjectPage.enterProf("Petrov");
        subjectPage.clickBtnEnter();
        subjectPage.loadList();

        int row = 1;
        while (!subjectPage.getRow(row).equals("")) {
            row++;
        }
        row--;

        Assert.assertEquals("SSPR", subjectPage.getRow(row));

        subjectPage.clickBtnDeleteRow(row);
        Alert alert = driver.switchTo().alert();
        alert.accept();

        Assert.assertNotEquals("SSPR", subjectPage.getRow(row));
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
